#include "diagnostic.h"

#include <sstream>
#include <utility>

#include "colors.h"
#include "lexer.h"
#include "mem_buffer.h"
#include "diagnostic_listener.h"

diagnostic::~diagnostic() = default;

diagnostic::diagnostic(const location& Location,
                       std::string Message,
                       std::string Line)
    : Location(Location)
    , Message(std::move(Message))
    , Line(std::move(Line))
{
}

warning::warning(const location& Location,
                 std::string Message,
                 std::string Line)
    : diagnostic(Location, std::move(Message), std::move(Line))
{
}

std::string
diagnostic::ToString() const
{
    return Bold(Location.ToString() + ":") + " " + Color(GetType() + ":") +
           " " + Message + "\n " + DisplayLine() + "\n " + Underline();
}

std::string
diagnostic::Underline() const
{
    lexer Lexer(std::make_unique<mem_buffer>(Line),
                std::make_unique<diagnostic_listener>());
    auto AllTokens = Lexer.All();
    std::ostringstream Result;

    for (const auto& Token : AllTokens)
    {
        if (Token.Location.X == Location.X)
        {
            Result << "^" << std::string(Token.Lexeme.size() - 1, '~');
            break;
        }
        else
            Result << std::string(Token.Lexeme.size(), ' ');
    }
    return Color(Result.str()) + "\n";
}

std::string
diagnostic::DisplayLine() const
{
    lexer Lexer(std::make_unique<mem_buffer>(Line),
                std::make_shared<diagnostic_listener>());
    auto AllTokens = Lexer.All();
    std::ostringstream Result;

    for (const auto& Token : AllTokens)
        if (Token.Location.X == Location.X)
            Result << Color(Token.Lexeme);
        else
            Result << Token.Lexeme;

    return Result.str();
}

std::string
warning::GetType() const
{
    return "warning";
}
std::string
warning::Color(const std::string& Text) const
{
    return Magenta(Text);
}

std::string
error::GetType() const
{
    return "error";
}

error::error(const location& Location, std::string Message, std::string Line)
    : diagnostic(Location, std::move(Message), std::move(Line))
{
}

std::string
error::Color(const std::string& Text) const
{
    return Red(Text);
}
