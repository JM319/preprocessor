#ifndef C_DIAGNOSTIC_LISTENER_H
#define C_DIAGNOSTIC_LISTENER_H

#include <memory>
#include <vector>

class diagnostic;

class diagnostic_listener
{
public:
    virtual ~diagnostic_listener() = default;
    virtual void Add(std::unique_ptr<diagnostic>);

    [[nodiscard]] unsigned
    ErrorCount() const;
    [[nodiscard]] unsigned
    WarningCount() const;
    virtual void
    Flush(std::ostream&) const;

    [[nodiscard]] std::string
    ToString() const;

private:
    std::vector<std::unique_ptr<diagnostic>> Diagnostics;
    unsigned NumWarnings = 0;
    unsigned NumErrors = 0;
};

class null_listener : public diagnostic_listener
{
    void Add(std::unique_ptr<diagnostic>) override {}
    void
    Flush(std::ostream&) const override
    {
    }
};

#endif  // C_DIAGNOSTIC_LISTENER_H
