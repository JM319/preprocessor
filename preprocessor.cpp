#include "preprocessor.h"

#include <cassert>
#include <memory>
#include <optional>
#include <utility>

#include "colors.h"
#include "diagnostic_listener.h"
#include "directive_parser.h"
#include "lexer.h"
#include "mem_buffer.h"

bool
define_directive::Validate(
        const std::string& Line,
        const std::shared_ptr<diagnostic_listener>& Listener) const
{
    if (Identifier.Lexeme.empty())
    {
        Listener->Add(std::make_unique<error>(
                Identifier.Location,
                "no macro name given in #define directive",
                Line));
        return false;
    }
    if (Identifier.Type != token_type::IDENTIFIER)
    {
        Listener->Add(std::make_unique<error>(
                Identifier.Location, "macro names must be identifiers", Line));
        return false;
    }
    return true;
}

preprocessor::preprocessor(std::unique_ptr<scanner_interface> Lexer,
                           std::shared_ptr<diagnostic_listener> Listener)
    : mLexer(std::move(Lexer))
    , mListener(std::move(Listener))
{
}

bool
preprocessor::StartingPPDirective() const
{
    assert(mCurrentLine);
    auto Token = mCurrentLine->NextNonWhitespace();
    const auto Result = Token.Type == token_type::HASH;
    mCurrentLine->Rewind();
    return Result;
}

void
preprocessor::ProcessCurrentDirective()
{
    directive_parser Parser(mListener, std::move(mCurrentLine));
    const auto Directive = Parser.DirectiveToken();

    if (Directive.Lexeme == "error")
        mListener->Add(std::make_unique<error>(
                Directive.Location, Parser.Line, Parser.Line));
    else if (Directive.Lexeme == "line")
    {
        std::optional<int> LineNumber = Parser.LineDirective();
        if (LineNumber.has_value()) mLexer->SetLineNumber(LineNumber.value());
    }
}

void
preprocessor::ReadNextLineOfOutputTokens()
{
    mCurrentLine = mLexer->GetLineScanner();
    while (StartingPPDirective())
    {
        ProcessCurrentDirective();
        mCurrentLine = mLexer->GetLineScanner();
    }
}

token
preprocessor::Next()
{
    if (!mCurrentLine) ReadNextLineOfOutputTokens();
    if (!mCurrentLine) return mLexer->Next();
    const token Result = mCurrentLine->Next();
    if (Result.Type == token_type::EOS) mCurrentLine.reset();
    return Result;
}
