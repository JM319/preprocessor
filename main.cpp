#include <iostream>

#include "diagnostic_listener.h"
#include "lexer.h"
#include "mem_buffer.h"

struct options
{
    bool Trigraphs = false;
    bool Wtrigraphs = false;
    std::string InputFile;
};

void
parse_options(int Argc, char** Argv, options& Options)
{
    for (int ArgIndex = 1; ArgIndex < Argc; ++ArgIndex)
    {
        const std::string Argument = Argv[ArgIndex];
        if (Argument[0] == '-')
        {
            if (Argument == "-trigraphs")
                Options.Trigraphs = true;
            else if (Argument == "-Wtrigraphs")
                Options.Wtrigraphs = true;
            else
            {
                std::cerr << "Unrecognized Argument: " << Argument << "\n";
                exit(-1);
            }
        }
        else
        {
            // TODO: Need to handle multiple input files
            Options.InputFile = Argument;
        }
    }
}

int
main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << "[flags] <pathname>\n";
        return 1;
    }

    options Options;
    parse_options(argc, argv, Options);

    auto Listener = std::make_shared<diagnostic_listener>();

    lexer Lexer(std::make_unique<concat_backslash_newline_buffer>(
                        std::make_unique<trigraph_buffer>(
                                mem_buffer::FromFile(Options.InputFile.c_str()),
                                Options.Wtrigraphs
                                        ? Listener
                                        : std::make_shared<null_listener>(),
                                Options.Trigraphs)),
                Listener);

    const auto Tokens = Lexer.All();
    for (const auto& Token : Tokens)
        std::cout << Token.Lexeme;
    Listener->Flush(std::cerr);
    std::cerr << "\n";
    return 0;
}
