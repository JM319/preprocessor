import filecmp
import os
import re
import subprocess
import sys


def show_diff(lhs, rhs):
    with open(lhs, 'r') as f:
        lhs = f.read()
    with open(rhs, 'r') as f:
        rhs = f.read()

    print('{} vs {}'.format(lhs, rhs))
    # for a, b in zip(lhs, rhs):
    #     if a != b:
    #         print('\n mismatch: {} vs {}'.format(a, b))
    #     else:
    #         sys.stderr.write(a)


def remove_ansi_escapes(text):
    # https://stackoverflow.com/a/14693789
    ansi_escape_8bit = re.compile(
        br'(?:\x1B[@-Z\\-_]|[\x80-\x9A\x9C-\x9F]|(?:\x1B\[|\x9B)[0-?]*[ -/]*[@-~])'
    )

    return ansi_escape_8bit.sub(b'', text)


def compare_files(lhs, rhs):
    with open(lhs, 'r') as f:
        lhstext = f.read()
    with open(rhs, 'r') as f:
        rhstext = f.read()
    rhstext = remove_ansi_escapes(rhstext).strip()
    lhstext = remove_ansi_escapes(lhstext).strip()
    rhs_lines = rhstext.split('\n')
    lhs_lines = lhstext.split('\n')
    for rhs, lhs in zip(rhs_lines, lhs_lines):
        rhs = rhs.strip()
        lhs = lhs.strip()
        if rhs != lhs:
            return False
    return True


FLAGS = [
    [],
    ['-Wtrigraphs'],
    ['-trigraphs'],
    ['-trigraphs', '-Wtrigraphs']
]


class TestSpec:
    def __init__(self, src_text, output_text, error_text=None, flags=None):
        self.src_text = src_text
        self.output_text = output_text
        self.error_text = error_text
        self.flags = flags

    def run(self, program, flags):
        with open('test_out', 'w') as fout, open('test_err', 'w') as ferr:
            subprocess.call([program, self.src_text] + flags,
                            stdout=fout, stderr=ferr)
        result = True
        if not filecmp.cmp('test_out', self.output_text, False):
            print('Output differs!')
            show_diff(self.output_text, 'test_out')
            result = False
        os.remove('test_out')
        if self.error_text:
            if not compare_files('test_err', self.error_text):
                print('Error differs!')
                show_diff(self.error_text, 'test_err')
                result = False
            os.remove('test_err')
        return result


if __name__ == '__main__':
    program = sys.argv[1]
    cwd = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    success = True
    for i in range(4):  # TODO: Don't hardcode this
        basename = 'test{}'.format(i)
        spec = TestSpec('{}.in'.format(basename),
                        '{}.out'.format(basename),
                        '{}.err'.format(basename))
        success = spec.run(program, FLAGS[i]) and success
    if success:
        sys.exit(0)
    else:
        sys.exit(1)
