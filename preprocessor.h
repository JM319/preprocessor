#ifndef C_PREPROCESSOR_H
#define C_PREPROCESSOR_H

#include <memory>

#include "lexer.h"

struct define_directive
{
    token Identifier;
    std::vector<token> ReplacementList;
    std::vector<token> IdentifierList;

    [[nodiscard]] bool
    Validate(const std::string& Line,
             const std::shared_ptr<diagnostic_listener>& Listener) const;
};

class preprocessor
{
public:
    preprocessor(std::unique_ptr<scanner_interface> Lexer,
                 std::shared_ptr<diagnostic_listener> Listener);

    token
    Next();

private:
    void
    ReadNextLineOfOutputTokens();

    [[nodiscard]] bool
    StartingPPDirective() const;

    void
    ProcessCurrentDirective();

    std::unique_ptr<scanner_interface> mLexer;
    std::shared_ptr<diagnostic_listener> mListener;

    std::unique_ptr<scanner_interface> mCurrentLine;
};

#endif  // C_PREPROCESSOR_H
