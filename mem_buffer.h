#ifndef C_MEM_BUFFER_H
#define C_MEM_BUFFER_H

#include <memory>
#include <stack>
#include <string>
#include <utility>

#include "diagnostic.h"
#include "diagnostic_listener.h"
#include "location.h"

class buffer
{
public:
    explicit buffer(const location& Location);

    buffer(const buffer&) = delete;

    buffer&
    operator=(const buffer&) = delete;

    [[nodiscard]] virtual location
    GetLocation() const;

    [[nodiscard]] char
    Peek();

    virtual char
    Next() = 0;

    [[nodiscard]] virtual std::string
    GetLine(unsigned long) const = 0;

    // -------------------------------------------------------------------------
    // Routines for managing state
    // -------------------------------------------------------------------------
    virtual void
    Mark() = 0;

    virtual void
    Reset() = 0;

    virtual void
    PopMark() = 0;

    virtual void
    SetLineNumber(int LineNumber) = 0;

    virtual void
    Rewind() = 0;

protected:
    location mLocation;

    struct state
    {
        unsigned long Position;
        location Location;
    };
    std::stack<state> mSavedState;
};

class mem_buffer : public buffer
{
public:
    explicit mem_buffer(std::string Data, const char* Pathname = "<memory>");

    static std::unique_ptr<buffer>
    FromFile(const char* Pathname);

    mem_buffer(const mem_buffer&) = delete;
    mem_buffer&
    operator=(const mem_buffer&) = delete;

    char
    Next() override;

    [[nodiscard]] std::string
    GetLine(unsigned long LineNumber) const override;

    // -------------------------------------------------------------------------
    // Routines for managing state
    // -------------------------------------------------------------------------
    void
    Mark() override;

    void
    Reset() override;

    void
    PopMark() override;

    void
    SetLineNumber(int LineNumber) override;

    void
    Rewind() override;

private:
    const std::string mData;
    unsigned long mIndex;
};

class buffer_decorator : public buffer
{
public:
    explicit buffer_decorator(std::unique_ptr<buffer> Base);

    char
    Next() override;

    [[nodiscard]] std::string
    GetLine(unsigned long) const override;

    // -------------------------------------------------------------------------
    // Routines for managing state
    // -------------------------------------------------------------------------
    void
    Mark() override;

    void
    Reset() override;

    void
    PopMark() override;

    [[nodiscard]] location
    GetLocation() const override;

    void
    SetLineNumber(int LineNumber) override;

    void
    Rewind() override;

private:
    std::unique_ptr<buffer> mBase;
};

class concat_backslash_newline_buffer : public buffer_decorator
{
public:
    explicit concat_backslash_newline_buffer(std::unique_ptr<buffer> Base)
        : buffer_decorator(std::move(Base))
    {
    }

    char
    Next() override;
};

class trigraph_buffer : public buffer_decorator
{
public:
    explicit trigraph_buffer(std::unique_ptr<buffer> Base,
                             std::shared_ptr<diagnostic_listener> Listener =
                                     std::make_shared<diagnostic_listener>(),
                             bool DoReplacement = true)
        : buffer_decorator(std::move(Base))
        , mListener(std::move(Listener))
        , mDoReplacement(DoReplacement)
    {
    }

    char
    Next() override;

private:
    std::shared_ptr<diagnostic_listener> mListener;
    const bool mDoReplacement;
};

#endif
