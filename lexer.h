#ifndef C_LEXER_H
#define C_LEXER_H

#include <memory>
#include <string>
#include <vector>

#include "location.h"

class buffer;
class diagnostic_listener;

enum class token_type
{
    IDENTIFIER,
    PP_NUMBER,
    SPACE,
    NEWLINE,
    STRING_LITERAL,
    CHAR_CONSTANT,

    // Punctuators
    RSQUARE,
    LSQUARE,
    RPAREN,
    LPAREN,
    RCURLY,
    LCURLY,
    COMMA,
    COLON,
    EQ,
    HASH,
    MINUS,
    PLUS,
    STAR,
    DOT,
    DOTS,
    EOS,
    ERROR
};

struct token
{
    token_type Type;
    std::string Lexeme;
    location Location;
};

class scanner_interface
{
public:
    virtual ~scanner_interface();

    virtual token
    Next() = 0;

    token
    NextNonWhitespace();

    std::vector<token>
    All();

    std::vector<token>
    ReadLine();

    virtual void
    Rewind() = 0;

    virtual void
    SetLineNumber(int LineNumber) = 0;

    std::unique_ptr<scanner_interface>
    GetLineScanner();

    std::string ToString();
};

class scanner : public scanner_interface
{
public:
    explicit scanner(std::vector<token> Tokens);

    token
    Next() override;

    void
    Rewind() override;

    void
    SetLineNumber(int LineNumber) override;

private:
    [[nodiscard]] token
    GetEosToken() const;

    std::vector<token> mTokens;
    size_t mIndex;
};

class lexer : public scanner_interface
{
public:
    lexer(std::unique_ptr<buffer> Buffer,
          std::shared_ptr<diagnostic_listener> Listener);

    token
    Next() override;

    void
    SetLineNumber(int LineNumber) override;

    void
    Rewind() override;

private:
    bool
    CheckNext(char C);

    token
    ConstructToken(token_type Type, const std::string& Lexeme);

    std::string
    CharSequence(const std::string& Initial, char Delimiter);

    token
    StringLiteral(const std::string& Initial);

    token
    CharConstant(const std::string& Initial);

    void
    EscapeSequence(std::string& Lexeme);

    void
    EmitError(const std::string& Message);

    void
    EmitWarning(const std::string& Message);

    std::unique_ptr<buffer> mBuffer;
    std::shared_ptr<diagnostic_listener> mListener;
    location mLocation;
};

#endif  // C_LEXER_H
