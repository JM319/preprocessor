#include "mem_buffer.h"

#include <cassert>
#include <fstream>
#include <string_view>
#include <utility>

#include "diagnostic.h"

namespace
{
const char*
GetStartOfLine(const std::string& Data, unsigned long LineNumber)
{
    const char* C = Data.data();
    if (LineNumber == 1) return C;

    unsigned long CurrentLine = 1;
    for (; *C; ++C)
        if (*C == '\n' && LineNumber == ++CurrentLine) return ++C;
    return nullptr;
}
}

buffer::buffer(const location& Location)
    : mLocation(Location)
{
}

char
buffer::Peek()
{
    // I assume this will kill performance
    // Is there some way to cache the next char?
    Mark();
    const char Result = Next();
    Reset();
    return Result;
}

std::unique_ptr<buffer>
mem_buffer::FromFile(const char* Pathname)
{
    std::ifstream Stream(Pathname);
    if (Stream.fail()) return nullptr;

    // https://stackoverflow.com/a/2912614
    std::string Data((std::istreambuf_iterator<char>(Stream)),
                     (std::istreambuf_iterator<char>()));

    return std::make_unique<mem_buffer>(Data, Pathname);
}

location
buffer::GetLocation() const
{
    return mLocation;
}

mem_buffer::mem_buffer(std::string Data, const char* Pathname)
    : buffer(location(Pathname))
    , mData(std::move(Data))
    , mIndex(0)
{
}

void mem_buffer::Rewind()
{
    mIndex = 0;
    mLocation.X = 1;
    mLocation.Y = 1;
}

void
mem_buffer::SetLineNumber(int LineNumber)
{
    mLocation.Y = LineNumber;
}

char
mem_buffer::Next()
{
    const char C = mData[mIndex];
    mLocation = mLocation.Advance(C);

    if (C)
        return mData[mIndex++];
    else
        return '\0';
}

void
mem_buffer::Mark()
{
    mSavedState.push({mIndex, mLocation});
}

void
mem_buffer::Reset()
{
    assert(!mSavedState.empty());
    state State = mSavedState.top();
    mSavedState.pop();
    mLocation = State.Location;
    mIndex = State.Position;
}

std::string
mem_buffer::GetLine(unsigned long LineNumber) const
{
    const char* Start = GetStartOfLine(mData, LineNumber);
    if (Start == nullptr) return "";
    const char* End = Start;
    for (; *End; ++End)
        if (*End == '\n') break;
    return std::string(Start, End);
}

void
mem_buffer::PopMark()
{
    mSavedState.pop();
}

buffer_decorator::buffer_decorator(std::unique_ptr<buffer> Base)
    : buffer(Base->GetLocation())
    , mBase(std::move(Base))
{
}

void
buffer_decorator::Reset()
{
    mBase->Reset();
}

void buffer_decorator::Rewind()
{
    mBase->Rewind();
}

void
buffer_decorator::Mark()
{
    mBase->Mark();
}

std::string
buffer_decorator::GetLine(unsigned long LineNumber) const
{
    return mBase->GetLine(LineNumber);
}

char
buffer_decorator::Next()
{
    return mBase->Next();
}

location
buffer_decorator::GetLocation() const
{
    return mBase->GetLocation();
}

void
buffer_decorator::PopMark()
{
    mBase->PopMark();
}

void
buffer_decorator::SetLineNumber(int LineNumber)
{
    mBase->SetLineNumber(LineNumber);
}

char
concat_backslash_newline_buffer::Next()
{
    char Result = buffer_decorator::Next();
    if (Result == '\\' && buffer_decorator::Peek() == '\n')
    {
        buffer_decorator::Next();
        Result = buffer_decorator::Next();
    }
    return Result;
}

namespace
{
char
MapTrigraphTerminalToReplacement(char C)
{
    switch (C)
    {
        case '(':
            return '[';
        case ')':
            return ']';
        case '<':
            return '{';
        case '>':
            return '}';
        case '=':
            return '#';
        case '/':
            return '\\';
        case '\'':
            return '^';
        case '!':
            return '|';
        case '-':
            return '~';
        default:
            return '\0';
    }
}
}

char
trigraph_buffer::Next()
{
    // If we need to report a trigraph diagnostic, we'll want to show
    // the location at the beginning of the trigraph, so let's
    // go ahead and save it off.
    const location Location = GetLocation();

    char First = buffer_decorator::Next();
    if (First == '?')
    {
        Mark();
        char Second = buffer_decorator::Next();
        if (Second == '?')
        {
            const char Terminal = buffer_decorator::Next();
            const char ReplacementChar =
                    MapTrigraphTerminalToReplacement(Terminal);
            const std::string Replacement(1, ReplacementChar);
            if (ReplacementChar)
            {
                const std::string Trigraph = "??" + std::string(1, Terminal);
                if (mDoReplacement)
                {
                    mListener->Add(std::make_unique<warning>(
                            Location,
                            "trigraph " + Trigraph + " converted to " +
                                    Replacement + " [-Wtrigraphs]",
                            GetLine(Location.Y)));
                    PopMark();
                    return ReplacementChar;
                }
                else
                {
                    mListener->Add(std::make_unique<warning>(
                            Location,
                            "trigraph " + Trigraph +
                                    " ignored, use -trigraphs to enable "
                                    "[-Wtrigraphs]",
                            GetLine(Location.Y)));
                }
            }
        }
        Reset();
    }
    return First;
}
