#ifndef C_DIAGNOSTIC_H
#define C_DIAGNOSTIC_H

#include <memory>
#include <string>
#include <vector>

#include "location.h"

class diagnostic
{
public:
    virtual ~diagnostic();

    [[nodiscard]] virtual std::string
    GetType() const = 0;

    [[nodiscard]] std::string
    ToString() const;

protected:
    diagnostic(const location& Location, std::string Message, std::string Line);

    [[nodiscard]] virtual std::string
    Color(const std::string& Text) const = 0;

    const location Location;
    const std::string Message;
    const std::string Line;

private:
    [[nodiscard]] std::string
    Underline() const;

    [[nodiscard]] std::string
    DisplayLine() const;
};

class warning : public diagnostic
{
public:
    warning(const location& Location, std::string Message, std::string Line);

private:
    [[nodiscard]] std::string
    GetType() const override;

    [[nodiscard]] std::string
    Color(const std::string& Text) const override;
};

class error : public diagnostic
{
public:
    error(const location& Location, std::string Message, std::string Line);

    [[nodiscard]] std::string
    GetType() const override;

    [[nodiscard]] std::string
    Color(const std::string& Text) const override;
};
#endif  // C_DIAGNOSTIC_H
