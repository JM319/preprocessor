#ifndef C_DIRECTIVE_PARSER_H
#define C_DIRECTIVE_PARSER_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "lexer.h"

class diagnostic_listener;
class scanner_interface;
class define_directive;

class directive_parser
{
public:
    directive_parser(std::shared_ptr<diagnostic_listener> Listener,
                     std::unique_ptr<scanner_interface> Lexer);

    std::optional<int>
    LineDirective();

    std::unique_ptr<define_directive>
    DefineDirective();

    token DirectiveToken();

    const std::string Line;

private:
    void
    ParseMacroArguments(std::vector<token>& IdentifierList);

    void
    WarnAboutMissingSpaceAfterMacroName();

    void
    ParseReplacementList(std::vector<token>& ReplacementList);

private:
    std::shared_ptr<diagnostic_listener> Listener;
    std::unique_ptr<scanner_interface> Lexer;
    token Current;
};
#endif  // C_DIRECTIVE_PARSER_H
