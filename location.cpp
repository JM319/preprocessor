#include "location.h"

#include <cstring>
#include <sstream>

location::location(const char* Pathname, int X, int Y)
    : Pathname(Pathname)
    , X(X)
    , Y(Y)
{
}

location
location::Advance(char c) const
{
    if (c == '\n')
        return location(Pathname, 1, Y + 1);
    else if (c == '\0')
        return *this;
    else
        return location(Pathname, X + 1, Y);
}

location location::Advance(const std::string& String) const
{
    location Result = *this;
    for (char c : String)
        Result = Result.Advance(c);
    return Result;
}

bool
location::HasCoordinate(int TestX, int TestY) const
{
    return X == TestX && Y == TestY;
}

std::string
location::ToString() const
{
    std::ostringstream Result;
    Result << Pathname << ":" << Y << ":" << X;
    return Result.str();
}

bool
location::operator==(const location& Other) const
{
    return !(strcmp(Pathname, Other.Pathname)) && X == Other.X && Y == Other.Y;
}
