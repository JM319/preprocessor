#include "lexer.h"

#include <cassert>
#include <numeric>
#include <utility>

#include "diagnostic.h"
#include "diagnostic_listener.h"
#include "mem_buffer.h"

namespace
{
std::string
ToString(const std::vector<token>& Tokens)
{
    return std::accumulate(Tokens.begin(),
                           Tokens.end(),
                           std::string(""),
                           [](const std::string& lhs, const token& rhs) {
                             return lhs + rhs.Lexeme;
                           });
}
}

static inline bool
StartsIdentifier(char C)
{
    return isalpha(C) || C == '_';
}

static inline bool
IsInIdentifier(char C)
{
    return StartsIdentifier(C) || isdigit(C);
}

static inline bool
IsWhitespace(char C)
{
    return C == ' ' || C == '\t';
}

static inline bool
StartsNumber(char First, char Second)
{
    return isdigit(First) || (First == '.' && isdigit(Second));
}

static inline bool
IsInNumber(char C)
{
    return IsInIdentifier(C) || C == '.';
}

lexer::lexer(std::unique_ptr<buffer> Buffer,
             std::shared_ptr<diagnostic_listener> Listener)
    : mBuffer(std::move(Buffer))
    , mListener(std::move(Listener))
    , mLocation(mBuffer->GetLocation())
{
}

bool
lexer::CheckNext(char C)
{
    if (mBuffer->Peek() == C)
    {
        mBuffer->Next();
        return true;
    }
    return false;
}

token
lexer::ConstructToken(token_type Type, const std::string& Lexeme)
{
    const location Location = mLocation;
    mLocation = mBuffer->GetLocation();
    return {Type, Lexeme, Location};
}

void
lexer::EmitWarning(const std::string& Message)
{
    mListener->Add(std::make_unique<warning>(
            mLocation, Message, mBuffer->GetLine(mLocation.Y)));
}

void
lexer::EmitError(const std::string& Message)
{
    mListener->Add(std::make_unique<error>(
            mLocation, Message, mBuffer->GetLine(mLocation.Y)));
}

static bool
IsHexDigit(char Char)
{
    if (isdigit(Char)) return true;
    Char = (char)tolower((int)Char);
    return Char == 'a' || Char == 'b' || Char == 'c' || Char == 'd' ||
           Char == 'e' || Char == 'f';
}

std::string
lexer::CharSequence(const std::string& Initial, char Delimiter)
{
    std::string Lexeme = Initial;
    while (true)
    {
        // Break early if the next character will be invalid
        if (mBuffer->Peek() == '\n' || mBuffer->Peek() == '\0')
        {
            EmitWarning("missing terminating " + std::string(1, Delimiter) +
                        " character");
            return Lexeme;
        }

        const char Next = mBuffer->Next();
        Lexeme += Next;
        if (Next == '\\')
            EscapeSequence(Lexeme);
        else if (Next == Delimiter)
            break;
    }
    return Lexeme;
}

void
lexer::EscapeSequence(std::string& Lexeme)
{
    if (mBuffer->Peek() == '\n') return;
    const char First = mBuffer->Next();
    const char Second = mBuffer->Peek();

    Lexeme += First;

    if (std::string("'\"?\\abfnrtv").find(First) != std::string::npos) return;

    if (isdigit(First)) return;

    if (tolower(First) == 'x')
    {
        if (IsHexDigit(Second))
            Lexeme += mBuffer->Next();
        else
            EmitError("\\x used with no following hex digits");
        return;
    }
    EmitWarning("unknown escape sequence: '\\" + std::string(1, First) + "'");
}

token
lexer::CharConstant(const std::string& Initial)
{
    const std::string Lexeme = CharSequence(Initial, '\'');
    return ConstructToken(token_type::CHAR_CONSTANT, Lexeme);
}

token
lexer::StringLiteral(const std::string& Initial)
{
    const std::string Lexeme = CharSequence(Initial, '"');
    return ConstructToken(token_type::STRING_LITERAL, Lexeme);
}

token
lexer::Next()
{
    const char C = mBuffer->Next();
    if (C == 'L')
    {
        if (CheckNext('"')) return StringLiteral("L\"");
        if (CheckNext('\'')) return CharConstant("L'");
    }
    if (C == '"') return StringLiteral("\"");
    if (C == '\'') return CharConstant("'");

    if (StartsIdentifier(C))
    {
        std::string Lexeme(1, C);
        while (IsInIdentifier(mBuffer->Peek()))
            Lexeme += mBuffer->Next();
        return ConstructToken(token_type::IDENTIFIER, Lexeme);
    }
    if (IsWhitespace(C))
    {
        std::string Lexeme(1, C);
        while (IsWhitespace(mBuffer->Peek()))
            Lexeme += mBuffer->Next();
        return ConstructToken(token_type::SPACE, Lexeme);
    }

    if (StartsNumber(C, mBuffer->Peek()))
    {
        std::string Lexeme(1, C);
        while (true)
        {
            if (mBuffer->Peek() == 'e' || mBuffer->Peek() == 'E')
            {
                Lexeme += mBuffer->Next();
                if (CheckNext('+'))
                    Lexeme += "+";
                else if (CheckNext('-'))
                    Lexeme += "-";
            }
            else if (IsInNumber(mBuffer->Peek()))
                Lexeme += mBuffer->Next();
            else
                break;
        }
        return ConstructToken(token_type::PP_NUMBER, Lexeme);
    }
    switch (C)
    {
        case '\0':
            return ConstructToken(token_type::EOS, "");
        case '\n':
            return ConstructToken(token_type::NEWLINE, "\n");
        case '[':
            return ConstructToken(token_type::LSQUARE, "[");
        case ']':
            return ConstructToken(token_type::RSQUARE, "]");
        case '(':
            return ConstructToken(token_type::LPAREN, "(");
        case ')':
            return ConstructToken(token_type::RPAREN, ")");
        case '{':
            return ConstructToken(token_type::LCURLY, "{");
        case '}':
            return ConstructToken(token_type::RCURLY, "}");
        case ',':
            return ConstructToken(token_type::COMMA, ",");
        case ':':
            return ConstructToken(token_type::COLON, ":");
        case '=':
            return ConstructToken(token_type::EQ, "=");
        case '#':
            return ConstructToken(token_type::HASH, "#");
        case '+':
            return ConstructToken(token_type::PLUS, "+");
        case '-':
            return ConstructToken(token_type::MINUS, "-");
        case '*':
            return ConstructToken(token_type::STAR, "*");
        case '.':
            mBuffer->Mark();
            if (CheckNext('.'))
                if (CheckNext('.'))
                    return ConstructToken(token_type::DOTS, "...");
            mBuffer->Reset();
            return ConstructToken(token_type::DOT, ".");
        default:
            return ConstructToken(token_type::ERROR, std::string(1, C));
    }
}

std::vector<token>
scanner_interface::All()
{
    std::vector<token> Result;
    while (true)
    {
        const token Token = Next();
        if (Token.Type == token_type::EOS) break;
        Result.push_back(Token);
    }
    return Result;
}

std::vector<token>
scanner_interface::ReadLine()
{
    std::vector<token> Result;
    token Token = Next();
    while (!(Token.Type == token_type::NEWLINE ||
             Token.Type == token_type::EOS))
    {
        Result.push_back(Token);
        Token = Next();
    }
    Result.push_back(Token);
    return Result;
}

token
scanner_interface::NextNonWhitespace()
{
    token Result = Next();
    while (Result.Type == token_type::SPACE ||
           Result.Type == token_type::NEWLINE)
        Result = Next();
    return Result;
}

void
lexer::SetLineNumber(int LineNumber)
{
    mLocation.Y = LineNumber;
    mBuffer->SetLineNumber(LineNumber);
}

void
lexer::Rewind()
{
    mBuffer->Rewind();
}

scanner_interface::~scanner_interface() = default;

scanner::scanner(std::vector<token> Tokens)
    : mTokens(std::move(Tokens))
    , mIndex(0)
{
    if (mTokens.empty())
        throw std::runtime_error("Cannot scan over zero tokens");
}

token
scanner::GetEosToken() const
{
    const token& LastToken = mTokens[mTokens.size() - 1];
    token Result{
            token_type::EOS, "", LastToken.Location.Advance(LastToken.Lexeme)};
    return Result;
}

token
scanner::Next()
{
    if (mIndex == mTokens.size())
        return GetEosToken();
    else
        return mTokens[mIndex++];
}

void
scanner::SetLineNumber(int LineNumber)
{
    assert(LineNumber == 0);
    // TODO: If a scanner spans multiple lines, this will break
    for (size_t Index = mIndex; Index < static_cast<size_t>(LineNumber);
         ++Index)
        mTokens[mIndex].Location.Y = LineNumber;
}

void
scanner::Rewind()
{
    mIndex = 0;
}

std::unique_ptr<scanner_interface>
scanner_interface::GetLineScanner()
{
    return std::make_unique<scanner>(ReadLine());
}

std::string
scanner_interface::ToString()
{
    const auto Tokens = All();
    const std::string Line = ::ToString(Tokens);
    Rewind();
    return Line;
}
