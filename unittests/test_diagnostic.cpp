#include "../colors.h"
#include "../diagnostic.h"
#include "../diagnostic_listener.h"
#include "../location.h"
#include "gtest/gtest.h"

TEST(Diagnostic, PrintWarning)
{
    location Location("test.c", 3, 1);
    warning Warning(Location, "some diagnostic warning", "a line thing");
    const auto ExpectedResult = Bold("test.c:1:3:") + " " +
                                Magenta("warning:") +
                                " some diagnostic warning\n" + " " + "a " +
                                Magenta("line") + " thing" +
                                "\n"
                                " " +
                                Magenta("  ^~~~") + "\n";
    ASSERT_STREQ(Warning.ToString().c_str(), ExpectedResult.c_str());
}

TEST(Diagnostic, PrintError)
{
    location Location("test.c", 3, 1);
    error Error(Location, "some diagnostic warning", "a line thing");
    const auto ExpectedResult = Bold("test.c:1:3:") + " " + Red("error:") +
                                " some diagnostic warning\n" + " " + "a " +
                                Red("line") + " thing" +
                                "\n"
                                " " +
                                Red("  ^~~~") + "\n";
    ASSERT_STREQ(Error.ToString().c_str(), ExpectedResult.c_str());
}

TEST(Diagnostic, Listener)
{
    diagnostic_listener Listener;
    location Location("test.c", 3, 1);
    Listener.Add(std::make_unique<error>(Location, "bad", "a thing non"));
    Listener.Add(std::make_unique<warning>(Location, "oops", "!-oops"));
    ASSERT_EQ(Listener.ErrorCount(), 1);
    ASSERT_EQ(Listener.WarningCount(), 1);
}
