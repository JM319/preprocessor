#include "../location.h"
#include "gtest/gtest.h"

TEST(Location, InitLocation)
{
    location Location("Pathname");
    ASSERT_TRUE(Location.HasCoordinate(1, 1));
    ASSERT_EQ((Location.Pathname), "Pathname");
}

TEST(Location, Advance)
{
    location Location("Pathname");

    Location = Location.Advance('a');
    ASSERT_TRUE(Location.HasCoordinate(2, 1));

    Location = Location.Advance('\n');
    ASSERT_TRUE(Location.HasCoordinate(1, 2));

    Location = Location.Advance('\0');
    ASSERT_TRUE(Location.HasCoordinate(1, 2));
}
