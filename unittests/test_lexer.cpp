#include "../diagnostic.h"
#include "../diagnostic_listener.h"
#include "../lexer.h"
#include "../mem_buffer.h"
#include "gtest/gtest.h"

class Lex : public ::testing::Test
{
protected:
    void
    SetUp() override
    {
        Listener = std::make_shared<diagnostic_listener>();
    }

    lexer
    LexerFromString(const std::string& Input)
    {
        std::unique_ptr<buffer> BaseBuffer =
                std::make_unique<mem_buffer>(Input);
        std::unique_ptr<buffer> Wrapper =
                std::make_unique<concat_backslash_newline_buffer>(
                        std::move(BaseBuffer));
        return lexer(std::move(Wrapper), Listener);
    }

    void
    AssertErrorsContain(const std::string& ExpectedText)
    {
        std::stringstream Stream;
        Listener->Flush(Stream);
        Stream.seekg(0);
        const std::string Message = Stream.str();
        EXPECT_NE(Message.find(ExpectedText), std::string::npos);
    }

    std::shared_ptr<diagnostic_listener> Listener;
};

TEST_F(Lex, Identifiers)
{
    auto Lexer = LexerFromString("identifier a_b _thing9");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Type, token_type::IDENTIFIER);
    EXPECT_EQ(Token.Lexeme, "identifier");

    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::SPACE);
    EXPECT_TRUE(Token.Lexeme == " ");

    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::IDENTIFIER);
    EXPECT_TRUE(Token.Lexeme == "a_b");

    EXPECT_TRUE(Lexer.Next().Type == token_type::SPACE);
    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::IDENTIFIER);
    EXPECT_TRUE(Token.Lexeme == "_thing9");

    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::EOS);
    EXPECT_TRUE(Token.Lexeme.empty());
}

TEST_F(Lex, PreprocessingNumbers)
{
    auto Lexer = LexerFromString("1-.2 3..e- 3dE+ .2d-");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Type, token_type::PP_NUMBER);
    EXPECT_EQ(Token.Lexeme, "1");

    Token = Lexer.Next();
    EXPECT_EQ(Token.Type, token_type::MINUS);
    EXPECT_EQ(Token.Lexeme, "-");

    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::PP_NUMBER);
    EXPECT_TRUE(Token.Lexeme == ".2");

    Lexer.Next();
    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::PP_NUMBER);
    EXPECT_TRUE(Token.Lexeme == "3..e-");

    Lexer.Next();
    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::PP_NUMBER);
    EXPECT_TRUE(Token.Lexeme == "3dE+");

    Lexer.Next();
    Token = Lexer.Next();
    EXPECT_TRUE(Token.Type == token_type::PP_NUMBER);
    EXPECT_TRUE(Token.Lexeme == ".2d");

    EXPECT_TRUE(Lexer.Next().Type == token_type::MINUS);
}

TEST_F(Lex, Location)
{
    auto Lexer = LexerFromString("a de...-\n ab");
    token Token = Lexer.Next();
    EXPECT_TRUE(Token.Location.HasCoordinate(1, 1));
    location Location = Lexer.Next().Location;
    EXPECT_TRUE(Location.HasCoordinate(2, 1));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(3, 1));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(5, 1));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(8, 1));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(9, 1));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(1, 2));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(2, 2));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(4, 2));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(4, 2));
    EXPECT_TRUE(Lexer.Next().Location.HasCoordinate(4, 2));
}

TEST_F(Lex, Punctuators)
{
    // TODO: There are a lot more
    auto Lexer = LexerFromString(".+...*[](){},:=#");
    EXPECT_TRUE(Lexer.Next().Type == token_type::DOT);
    EXPECT_TRUE(Lexer.Next().Type == token_type::PLUS);
    EXPECT_TRUE(Lexer.Next().Type == token_type::DOTS);
    EXPECT_TRUE(Lexer.Next().Type == token_type::STAR);
    EXPECT_TRUE(Lexer.Next().Type == token_type::LSQUARE);
    EXPECT_TRUE(Lexer.Next().Type == token_type::RSQUARE);
    EXPECT_TRUE(Lexer.Next().Type == token_type::LPAREN);
    EXPECT_TRUE(Lexer.Next().Type == token_type::RPAREN);
    EXPECT_TRUE(Lexer.Next().Type == token_type::LCURLY);
    EXPECT_TRUE(Lexer.Next().Type == token_type::RCURLY);
    EXPECT_TRUE(Lexer.Next().Type == token_type::COMMA);
    EXPECT_TRUE(Lexer.Next().Type == token_type::COLON);
    EXPECT_TRUE(Lexer.Next().Type == token_type::EQ);
    EXPECT_TRUE(Lexer.Next().Type == token_type::HASH);
}

TEST_F(Lex, StringLiterals)
{
    auto Lexer =
            LexerFromString(R"("ab cd" "a\"a" L"ab" "a\03" "B\x03" "\xg")");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Lexeme, "\"ab cd\"");
    EXPECT_EQ(Token.Type, token_type::STRING_LITERAL);

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "\"a\\\"a\"");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "L\"ab\"");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "\"a\\03\"");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "\"B\\x03\"");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "\"\\xg\"");

    AssertErrorsContain("\\x used with no following hex digits");

    EXPECT_EQ(Listener->ErrorCount(), 1);
    EXPECT_EQ(Listener->WarningCount(), 0);
}

TEST_F(Lex, CharConstants)
{
    auto Lexer = LexerFromString(R"('ab cd' L'ab' 'B\x03' '\xg')");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Lexeme, "'ab cd'");
    EXPECT_EQ(Token.Type, token_type::CHAR_CONSTANT);

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "L'ab'");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "'B\\x03'");

    Lexer.Next();
    EXPECT_EQ(Lexer.Next().Lexeme, "'\\xg'");

    AssertErrorsContain("\\x used with no following hex digits");

    EXPECT_EQ(Listener->ErrorCount(), 1);
    EXPECT_EQ(Listener->WarningCount(), 0);
}

TEST_F(Lex, UnterminatedString)
{
    auto Lexer = LexerFromString(R"("ab
)");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Lexeme, R"("ab)");
    EXPECT_EQ(Token.Type, token_type::STRING_LITERAL);
    EXPECT_EQ(Lexer.Next().Type, token_type::NEWLINE);

    AssertErrorsContain("missing terminating \" character");
}

TEST_F(Lex, UnterminatedCharConstant)
{
    auto Lexer = LexerFromString(R"('ab
)");
    token Token = Lexer.Next();
    EXPECT_EQ(Token.Lexeme, R"('ab)");
    EXPECT_EQ(Token.Type, token_type::CHAR_CONSTANT);
    EXPECT_EQ(Lexer.Next().Type, token_type::NEWLINE);

    AssertErrorsContain("missing terminating ' character");
}

TEST_F(Lex, EscapeSequences)
{
    auto Lexer = LexerFromString(R"("\'\?")");
    token Token = Lexer.Next();
    EXPECT_TRUE(Token.Lexeme == R"("\'\?")");
    EXPECT_TRUE(Token.Type == token_type::STRING_LITERAL);
}

TEST_F(Lex, InvalidEscapeSequence)
{
    EXPECT_EQ(Listener->WarningCount(), 0);
    auto Lexer = LexerFromString(R"("\d")");
    const auto Token = Lexer.Next();
    EXPECT_EQ(Token.Lexeme, "\"\\d\"");
    EXPECT_TRUE(Token.Type == token_type::STRING_LITERAL);
    EXPECT_EQ(Listener->WarningCount(), 1);

    AssertErrorsContain("\\d");
    AssertErrorsContain("unknown escape sequence");
}

TEST_F(Lex, IgnoreWhitespace)
{
    auto Lexer = LexerFromString("a b c");
    EXPECT_EQ(Lexer.NextNonWhitespace().Lexeme, "a");
    EXPECT_EQ(Lexer.NextNonWhitespace().Lexeme, "b");
    EXPECT_EQ(Lexer.NextNonWhitespace().Lexeme, "c");
    EXPECT_EQ(Lexer.NextNonWhitespace().Type, token_type::EOS);
}

TEST_F(Lex, ReadLine)
{
    auto Lexer = LexerFromString("a b\nc d");
    const auto FirstLine = Lexer.ReadLine();
    EXPECT_EQ(FirstLine.size(), 4);
    EXPECT_EQ(FirstLine[2].Lexeme, "b");
    EXPECT_EQ(FirstLine[3].Type, token_type::NEWLINE);

    EXPECT_EQ(Lexer.Next().Lexeme, "c");
    const auto SecondLine = Lexer.ReadLine();
    EXPECT_EQ(Lexer.Next().Type, token_type::EOS);
}

TEST_F(Lex, ConcatBackslashNewline)
{
    auto Lexer = LexerFromString(R"(a\
c d)");
    const auto FirstLine = Lexer.ReadLine();
    EXPECT_EQ(FirstLine.size(), 4);
    EXPECT_EQ(FirstLine[0].Lexeme, "ac");
    EXPECT_EQ(FirstLine[1].Lexeme, " ");
    EXPECT_EQ(FirstLine[2].Lexeme, "d");
}

TEST_F(Lex, BackslashNewlineInStringLiteral)
{
    auto Lexer = LexerFromString(R"("st\
ring")");
    const auto FirstLine = Lexer.ReadLine();
    EXPECT_EQ(FirstLine.size(), 2);
    EXPECT_EQ(FirstLine[0].Lexeme, "\"string\"");
}

TEST_F(Lex, ChangeLineNumber)
{
    auto Lexer = LexerFromString("a b c");
    auto Token = Lexer.Next();
    EXPECT_EQ(Token.Location.Y, 1);
    Lexer.SetLineNumber(5);
    Token = Lexer.Next();
    EXPECT_EQ(Token.Location.Y, 5);
}

TEST_F(Lex, ScannerIteratesOverTokens)
{
    auto SourceLexer = LexerFromString("a b c\n");
    const auto Line = SourceLexer.ReadLine();
    scanner Scanner(Line);
    EXPECT_STREQ(Scanner.Next().Lexeme.c_str(), "a");
    Scanner.Next();
    EXPECT_STREQ(Scanner.Next().Lexeme.c_str(), "b");
    Scanner.Next();
    EXPECT_STREQ(Scanner.Next().Lexeme.c_str(), "c");
    EXPECT_EQ(Scanner.Next().Type, token_type::NEWLINE);
    EXPECT_EQ(Scanner.Next().Type, token_type::EOS);
}

TEST_F(Lex, GetScannerFromLine)
{
    auto SourceLexer = LexerFromString("a b c\nd e f\nh");
    auto FirstLineScanner = SourceLexer.GetLineScanner();
    EXPECT_STREQ(FirstLineScanner->Next().Lexeme.c_str(), "a");
    auto SecondLineScanner = SourceLexer.GetLineScanner();
    EXPECT_STREQ(SecondLineScanner->Next().Lexeme.c_str(), "d");
    EXPECT_STREQ(SourceLexer.Next().Lexeme.c_str(), "h");
}

TEST_F(Lex, ScannerFromEmptyLine)
{
    auto SourceLexer = LexerFromString("a b c\n\n");
    auto FirstLineScanner = SourceLexer.GetLineScanner();
    auto SecondLineScanner = SourceLexer.GetLineScanner();
    EXPECT_EQ(SecondLineScanner->Next().Type, token_type::NEWLINE);
    EXPECT_EQ(SecondLineScanner->Next().Type, token_type::EOS);
}

TEST_F(Lex, CannotBuildScannerFromEmptyVector)
{
    EXPECT_ANY_THROW(scanner Scanner({}));
}

TEST_F(Lex, RewindLexer)
{
    auto SourceLexer = LexerFromString("a b c\nd e f\nh");
    SourceLexer.Next();
    SourceLexer.Next();
    SourceLexer.Rewind();
    ASSERT_STREQ(SourceLexer.Next().Lexeme.c_str(), "a");
}

TEST_F(Lex, RewindScanner)
{
    auto SourceLexer = LexerFromString("a b c\nd e f\nh");
    auto FirstLineScanner = SourceLexer.GetLineScanner();
    FirstLineScanner->All();
    FirstLineScanner->Rewind();
    ASSERT_STREQ(FirstLineScanner->Next().Lexeme.c_str(), "a");
}