#include <gtest/gtest.h>

#include "../colors.h"
#include "../directive_parser.h"
#include "../lexer.h"
#include "../mem_buffer.h"
#include "../preprocessor.h"

class TestPreprocessor : public testing::Test
{
protected:
    void
    SetUp() override
    {
        Listener = std::make_shared<diagnostic_listener>();
    }

    std::unique_ptr<scanner_interface>
    ConstructLexer(const std::string& Input)
    {
        std::unique_ptr<buffer> BaseBuffer =
                std::make_unique<mem_buffer>(Input);
        std::unique_ptr<buffer> Wrapper =
                std::make_unique<concat_backslash_newline_buffer>(
                        std::move(BaseBuffer));
        return std::make_unique<lexer>(std::move(Wrapper), Listener);
    }

    std::unique_ptr<preprocessor>
    ConstructPreprocessor(const std::string& Input)
    {
        return std::make_unique<preprocessor>(ConstructLexer(Input), Listener);
    }

    bool
    ErrorMessageContains(const std::string& Excerpt)
    {
        const auto ErrorMessage = Listener->ToString();
        return ErrorMessage.find(Excerpt) != std::string::npos;
    }

    std::shared_ptr<diagnostic_listener> Listener;
};

TEST_F(TestPreprocessor, NextNormalToken)
{
    auto Preprocessor = ConstructPreprocessor("a");
    ASSERT_EQ(Preprocessor->Next().Lexeme, "a");
}

TEST_F(TestPreprocessor, DirectiveEmbeddedInLine)
{
    auto Preprocessor = ConstructPreprocessor("a#if 0");
    ASSERT_EQ(Preprocessor->Next().Lexeme, "a");
    ASSERT_EQ(Preprocessor->Next().Lexeme, "#");
    ASSERT_EQ(Preprocessor->Next().Lexeme, "if");
}

TEST_F(TestPreprocessor, DontEmitErrorToken)
{
    auto Preprocessor = ConstructPreprocessor("#error message\na");
    const token Token = Preprocessor->Next();
    ASSERT_STREQ(Token.Lexeme.c_str(), "a");
}

TEST_F(TestPreprocessor, RecognizePPDirectiveWithSpaces)
{
    auto Preprocessor = ConstructPreprocessor(" # if condition\na");
    const token Token = Preprocessor->Next();
    ASSERT_STREQ(Token.Lexeme.c_str(), "a");
}

TEST_F(TestPreprocessor, FinalLineIsPPDirective)
{
    auto Preprocessor = ConstructPreprocessor(" # define foo");
    const token Token = Preprocessor->Next();
    ASSERT_EQ(Token.Type, token_type::EOS);
}

TEST_F(TestPreprocessor, EmitErrorOnToken)
{
    auto Preprocessor = ConstructPreprocessor("#error message");
    Preprocessor->Next();
    ASSERT_TRUE(ErrorMessageContains("#error message")) << Listener->ToString();
}

TEST_F(TestPreprocessor, LineDirective)
{
    auto Preprocessor = ConstructPreprocessor("#line 234\na b");
    token Token = Preprocessor->Next();
    ASSERT_EQ(Token.Location.Y, 234);
}

TEST_F(TestPreprocessor, MissingLineNumberInDirective)
{
    auto Preprocessor = ConstructPreprocessor("#line\na");
    Preprocessor->Next();
    ASSERT_TRUE(ErrorMessageContains("unexpected end of file after #line"))
            << Listener->ToString();
}

TEST_F(TestPreprocessor, LineNumberIsNotANumber)
{
    auto Preprocessor = ConstructPreprocessor("#line foo\n");
    Preprocessor->Next();
    const auto ErrorMessage = Listener->ToString();
    ASSERT_TRUE(ErrorMessage.find(
                        "\"foo\" after #line is not a positive integer") !=
                std::string::npos)
            << ErrorMessage;
}

TEST_F(TestPreprocessor, LineNumberIsNegative)
{
    auto Preprocessor = ConstructPreprocessor("#line -1\n");
    Preprocessor->Next();
    ASSERT_TRUE(
            ErrorMessageContains("\"-\" after #line is not a positive integer"))
            << Listener->ToString();
}

TEST_F(TestPreprocessor, LineNumberIsFloat)
{
    auto Preprocessor = ConstructPreprocessor("#line 2.\n");
    Preprocessor->Next();
    ASSERT_TRUE(ErrorMessageContains(
            "\"2.\" after #line is not a positive integer"))
            << Listener->ToString();
}

class TestDirectiveParser : public ::testing::Test
{
protected:
    void
    SetUp() override
    {
        Listener = std::make_shared<diagnostic_listener>();
    }

    std::unique_ptr<scanner_interface>
    ConstructLexer(const std::string& Input)
    {
        std::unique_ptr<buffer> BaseBuffer =
                std::make_unique<mem_buffer>(Input);
        std::unique_ptr<buffer> Wrapper =
                std::make_unique<concat_backslash_newline_buffer>(
                        std::move(BaseBuffer));
        return std::make_unique<lexer>(std::move(Wrapper), Listener);
    }

    std::unique_ptr<directive_parser>
    PrepareParser(const std::string& Input)
    {
        auto Lexer = ConstructLexer(Input);
        auto Parser =
                std::make_unique<directive_parser>(Listener, std::move(Lexer));
        Parser->DirectiveToken();
        return Parser;
    }

    bool
    ErrorMessageContains(const std::string& Excerpt)
    {
        const auto ErrorMessage = Listener->ToString();
        return ErrorMessage.find(Excerpt) != std::string::npos;
    }

    std::shared_ptr<diagnostic_listener> Listener;
};

TEST_F(TestDirectiveParser, ParseSimpleDefineDirective)
{
    auto Parser = PrepareParser("#define two 3 four 5\n");
    auto Directive = Parser->DefineDirective();
    EXPECT_EQ(Directive->Identifier.Lexeme, "two");
    EXPECT_EQ(Directive->Identifier.Type, token_type::IDENTIFIER);
    EXPECT_EQ(Directive->ReplacementList.size(), 5);
    EXPECT_EQ(Directive->ReplacementList[0].Lexeme, "3");
    EXPECT_EQ(Directive->ReplacementList[4].Lexeme, "5");
}

TEST_F(TestDirectiveParser, ParseDefineDirectiveWithoutReplacementList)
{
    auto Parser = PrepareParser("#define two\n");
    auto Directive = Parser->DefineDirective();
    EXPECT_EQ(Directive->Identifier.Lexeme, "two");
    EXPECT_EQ(Directive->Identifier.Type, token_type::IDENTIFIER);
    EXPECT_TRUE(Directive->ReplacementList.empty());
}

TEST_F(TestDirectiveParser, BadDirectiveNoIdentifier)
{
    auto Parser = PrepareParser("#define \n");
    auto Directive = Parser->DefineDirective();
    EXPECT_TRUE(
            ErrorMessageContains("no macro name given in #define directive"))
            << Listener->ToString();
    EXPECT_FALSE(Directive);
}

TEST_F(TestDirectiveParser, BadDirectiveIdentifierWrongType)
{
    auto Parser = PrepareParser("#define 5 thing\n");
    auto Directive = Parser->DefineDirective();
    EXPECT_FALSE(Directive);
    EXPECT_TRUE(ErrorMessageContains("macro names must be identifiers"))
            << Listener->ToString();
    EXPECT_TRUE(ErrorMessageContains(Red("5"))) << Listener->ToString();
}

TEST_F(TestDirectiveParser, WarnNoWhitespaceAfterMacroName)
{
    auto Parser = PrepareParser("#define two.3\n");
    auto Directive = Parser->DefineDirective();
    EXPECT_EQ(Directive->Identifier.Lexeme, "two");
    EXPECT_EQ(Directive->Identifier.Type, token_type::IDENTIFIER);
    EXPECT_EQ(Directive->ReplacementList.size(), 1);
    EXPECT_TRUE(ErrorMessageContains("missing whitespace after the macro name"))
            << Listener->ToString();
    EXPECT_TRUE(ErrorMessageContains(Magenta(".3"))) << Listener->ToString();
}

TEST_F(TestDirectiveParser, EmptyIdentifierList)
{
    auto Parser = PrepareParser("#define a()\n");
    auto Directive = Parser->DefineDirective();
    ASSERT_TRUE(Directive);
    EXPECT_TRUE(Directive->IdentifierList.empty());
    EXPECT_TRUE(Directive->ReplacementList.empty())
            << Directive->ReplacementList[0].Lexeme;
    EXPECT_EQ(Directive->Identifier.Lexeme, "a");
}

TEST_F(TestPreprocessor, DefineMacroSingleArgument)
{
    auto Lexer = ConstructLexer("#define a(b) d e\n");
    directive_parser Parser(Listener, std::move(Lexer));
    Parser.DirectiveToken();
    auto Directive = Parser.DefineDirective();
    ASSERT_TRUE(Directive);
    ASSERT_EQ(Directive->IdentifierList.size(), 1);
    EXPECT_EQ(Directive->IdentifierList[0].Lexeme, "b");
}

TEST_F(TestPreprocessor, DefineMacroArguments)
{
    auto Lexer = ConstructLexer("#define a(b, d) d e\n");
    directive_parser Parser(Listener, std::move(Lexer));
    Parser.DirectiveToken();
    auto Directive = Parser.DefineDirective();
    ASSERT_TRUE(Directive);
    ASSERT_EQ(Directive->IdentifierList.size(), 2);
    EXPECT_EQ(Directive->IdentifierList[0].Lexeme, "b");
    EXPECT_EQ(Directive->IdentifierList[1].Lexeme, "d");
}
