#ifndef C_TEST_H
#define C_TEST_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define ASSERT(Expr)                                      \
    if (!(Expr))                                          \
    {                                                     \
        fprintf(stderr,                                   \
                "%s failed on line %d: %s is not true\n", \
                __PRETTY_FUNCTION__,                      \
                __LINE__,                                 \
                #Expr);                                   \
        exit(-1);                                         \
    }

#define ASSERT_STR_EQ(Lhs, Rhs)                     \
    if (strcmp((Lhs), (Rhs)) != 0)                  \
    {                                               \
        fprintf(stderr,                             \
                "%s failed on line %d: %s != %s\n", \
                __PRETTY_FUNCTION__,                \
                __LINE__,                           \
                Lhs,                                \
                Rhs);                               \
        exit(-1);                                   \
    }

#define ASSERT_NULL(Expr)                                 \
    if ((Expr) != nullptr)                                \
    {                                                     \
        fprintf(stderr,                                   \
                "[%s:%d]: %s is not NULL\n", \
                __FILE__,                                 \
                __LINE__,                                 \
                #Expr);                                   \
        exit(-1);                                         \
    }
#endif
