#include <fstream>

#include "../diagnostic_listener.h"
#include "../location.h"
#include "../mem_buffer.h"
#include "gtest/gtest.h"

TEST(Buffer, InitBuffer)
{
    mem_buffer Buffer("content");
    ASSERT_EQ(Buffer.Peek(), 'c');
}

TEST(Buffer, Advance)
{
    mem_buffer Buffer("content");
    ASSERT_EQ(Buffer.Next(), 'c');
    ASSERT_EQ(Buffer.Next(), 'o');
    ASSERT_EQ(Buffer.Peek(), 'n');
    ASSERT_EQ(Buffer.Peek(), 'n');
}

TEST(Buffer, EndOfBuffer)
{
    mem_buffer Buffer("a");
    ASSERT_EQ(Buffer.Next(), 'a');
    ASSERT_EQ(Buffer.Next(), '\0');
    ASSERT_EQ(Buffer.Next(), '\0');
    ASSERT_EQ(Buffer.Peek(), '\0');
}

TEST(Buffer, GetLocation)
{
    mem_buffer Buffer("a");
    location Location = Buffer.GetLocation();
    ASSERT_STREQ(Location.Pathname, "<memory>");
    ASSERT_EQ(Location.X, 1);
    ASSERT_EQ(Location.Y, 1);

    ASSERT_EQ(Buffer.Next(), 'a');
    Location = Buffer.GetLocation();
    ASSERT_EQ(Location.X, 2);
    ASSERT_EQ(Location.Y, 1);

    Buffer.Next();
    Location = Buffer.GetLocation();
    ASSERT_EQ(Location.X, 2);
    ASSERT_EQ(Location.Y, 1);
}

TEST(Buffer, MarkAndReset)
{
    mem_buffer Buffer("content");
    Buffer.Next();
    const location MarkedLocation = Buffer.GetLocation();
    const char MarkedChar = Buffer.Peek();
    Buffer.Mark();

    Buffer.Next();
    Buffer.Next();
    Buffer.Reset();

    ASSERT_EQ(MarkedChar, Buffer.Peek());
    const location ActualLocation = Buffer.GetLocation();
    ASSERT_EQ(MarkedLocation.X, ActualLocation.X);
    ASSERT_EQ(MarkedLocation.Y, ActualLocation.Y);
}

TEST(Buffer, GetLine)
{
    mem_buffer Buffer("1\n2 \n\n405");
    ASSERT_EQ(Buffer.GetLine(1), "1");
    ASSERT_EQ(Buffer.GetLine(2), "2 ");
    ASSERT_TRUE(Buffer.GetLine(3).empty());
    ASSERT_EQ(Buffer.GetLine(4), "405");
    ASSERT_TRUE(Buffer.GetLine(5).empty());
}

TEST(Buffer, FromFile)
{
    {
        std::ofstream Stream("test.txt");
        Stream << "content\n";
    }

    auto Buffer = mem_buffer::FromFile("test.txt");
    ASSERT_EQ(Buffer->Next(), 'c');
    ASSERT_EQ(Buffer->Next(), 'o');
    ASSERT_EQ(Buffer->Next(), 'n');
    ASSERT_EQ(Buffer->Next(), 't');

    remove("test.txt");
}

TEST(Buffer, FromNonexistingFile)
{
    auto Buffer = mem_buffer::FromFile("test.txt");
    ASSERT_FALSE(Buffer.get());
}

TEST(Buffer, NewlineDecorator)
{
    std::unique_ptr<buffer> Buffer = std::make_unique<mem_buffer>(R"(b\
cd)");
    concat_backslash_newline_buffer Decorated(std::move(Buffer));
    ASSERT_EQ(Decorated.Next(), 'b');
    ASSERT_EQ(Decorated.Next(), 'c');
}

TEST(Buffer, TrigraphBuffer)
{
    std::unique_ptr<buffer> Buffer =
            std::make_unique<mem_buffer>("??(??)??<??>??=??/??'??!??-??a");
    trigraph_buffer Decorated(std::move(Buffer));
    ASSERT_EQ(Decorated.Next(), '[');
    ASSERT_EQ(Decorated.Next(), ']');
    ASSERT_EQ(Decorated.Next(), '{');
    ASSERT_EQ(Decorated.Next(), '}');
    ASSERT_EQ(Decorated.Next(), '#');
    ASSERT_EQ(Decorated.Next(), '\\');
    ASSERT_EQ(Decorated.Next(), '^');
    ASSERT_EQ(Decorated.Next(), '|');
    ASSERT_EQ(Decorated.Next(), '~');
    ASSERT_EQ(Decorated.Next(), '?');
    ASSERT_EQ(Decorated.Next(), '?');
    ASSERT_EQ(Decorated.Next(), 'a');
}

TEST(Buffer, ComposeDecorators)
{
    std::unique_ptr<buffer> Buffer = std::make_unique<mem_buffer>(R"(b\
??()");
    std::unique_ptr<buffer> Decorated =
            std::make_unique<concat_backslash_newline_buffer>(
                    std::move(Buffer));
    trigraph_buffer TrigraphNewline(std::move(Decorated));
    ASSERT_EQ(TrigraphNewline.Next(), 'b');
    ASSERT_EQ(TrigraphNewline.Next(), '[');
}

TEST(Buffer, ComposeDecoratorsTheOtherWay)
{
    std::unique_ptr<buffer> Buffer = std::make_unique<mem_buffer>(R"(b\
??()");
    std::unique_ptr<buffer> Decorated =
            std::make_unique<trigraph_buffer>(std::move(Buffer));
    concat_backslash_newline_buffer TrigraphNewline(std::move(Decorated));
    ASSERT_EQ(TrigraphNewline.Next(), 'b');
    ASSERT_EQ(TrigraphNewline.Next(), '[');
}

TEST(Buffer, BackslashNewlineUsingTrigraphs)
{
    // NOTE: Buffer decorators MUST be composed in this order
    // Otherwise, this case will not work
    // TODO: Is there a way to enforce composing these correctly?
    std::unique_ptr<buffer> Buffer = std::make_unique<mem_buffer>(R"(b??/
a??( ab??()");
    std::unique_ptr<buffer> Decorated =
            std::make_unique<trigraph_buffer>(std::move(Buffer));
    concat_backslash_newline_buffer TrigraphNewline(std::move(Decorated));

    ASSERT_EQ(TrigraphNewline.Next(), 'b');
    ASSERT_EQ(TrigraphNewline.Next(), 'a');
    ASSERT_EQ(TrigraphNewline.Next(), '[');
    ASSERT_EQ(TrigraphNewline.Next(), ' ');
    ASSERT_EQ(TrigraphNewline.Next(), 'a');
    ASSERT_EQ(TrigraphNewline.Next(), 'b');
    ASSERT_EQ(TrigraphNewline.Next(), '[');
}

TEST(Buffer, DecoratorIgnoreAndWarnTrigraphs)
{
    auto Listener = std::make_shared<diagnostic_listener>();
    std::unique_ptr<buffer> Buffer =
            std::make_unique<mem_buffer>(R"(b??(a??))");
    std::unique_ptr<buffer> Decorated = std::make_unique<trigraph_buffer>(
            std::move(Buffer), Listener, false);

    ASSERT_EQ(Decorated->Next(), 'b');
    ASSERT_EQ(Decorated->Next(), '?');
    ASSERT_EQ(Decorated->Next(), '?');
    ASSERT_EQ(Decorated->Next(), '(');
    ASSERT_EQ(Decorated->Next(), 'a');
    ASSERT_EQ(Decorated->Next(), '?');

    std::stringstream ErrorStream;
    Listener->Flush(ErrorStream);
    ErrorStream.seekg(0);
    std::string Message = ErrorStream.str();
    ASSERT_TRUE(Message.find("trigraph ??( ignored") != std::string::npos);

    // Make sure we're pointing to the correct locations!
    ASSERT_TRUE(Message.find("1:2") != std::string::npos);
    ASSERT_TRUE(Message.find("1:6") != std::string::npos);
}

TEST(Buffer, AdjustCurrentLine)
{
    std::unique_ptr<buffer> Buffer = std::make_unique<mem_buffer>("ab\ncd");
    Buffer->SetLineNumber(5);
    ASSERT_EQ(Buffer->GetLocation().Y, 5);
}
