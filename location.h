#ifndef C_LOCATION_H
#define C_LOCATION_H

#include <string>
struct location
{
    explicit location(const char* Pathname, int X = 1, int Y = 1);

    bool operator==(const location&) const;

    [[nodiscard]] location
    Advance(char c) const;

    [[nodiscard]] location
    Advance(const std::string& String) const;

    [[nodiscard]] bool
    HasCoordinate(int x, int y) const;

    const char* Pathname;
    int X, Y;

    [[nodiscard]] std::string
    ToString() const;
};

#endif
