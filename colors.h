#ifndef C_COLORS_H
#define C_COLORS_H

#include <string>

inline std::string
Magenta(const std::string& String)
{
    return "\u001b[35m" + String + "\u001b[0m";
}

inline std::string
Red(const std::string& String)
{
    return "\u001b[31m" + String + "\u001b[0m";
}

inline std::string
Bold(const std::string& String)
{
    return "\u001b[1m" + String + "\u001b[0m";
}

inline std::string
Quote(const std::string& String)
{
    return "\"" + String + "\"";
}

#endif  // C_COLORS_H
