#include "directive_parser.h"

#include <cassert>

#include "colors.h"
#include "diagnostic.h"
#include "diagnostic_listener.h"
#include "preprocessor.h"

namespace
{
bool
ParsePositiveInteger(const std::string& Lexeme, int& Number)
{
    Number = 0;
    for (char c : Lexeme)
    {
        if (!isdigit(c)) return false;
        Number = Number * 10 + (c - '0');
    }
    return true;
}
}

directive_parser::directive_parser(
        std::shared_ptr<diagnostic_listener> Listener,
        std::unique_ptr<scanner_interface> Lexer)
    : Line(Lexer->ToString())
    , Listener(std::move(Listener))
    , Lexer(std::move(Lexer))
    , Current(this->Lexer->NextNonWhitespace())
{
}

token
directive_parser::DirectiveToken()
{
    const auto Hash = Current;
    assert(Hash.Type == token_type::HASH);
    return Lexer->NextNonWhitespace();
}

std::unique_ptr<define_directive>
directive_parser::DefineDirective()
{
    const auto Identifier = Lexer->NextNonWhitespace();
    auto Result = std::make_unique<define_directive>(
            define_directive{Identifier, {}, {}});
    Current = Identifier;
    WarnAboutMissingSpaceAfterMacroName();
    ParseMacroArguments(Result->IdentifierList);
    ParseReplacementList(Result->ReplacementList);

    return Result->Validate(Line, Listener) ? std::move(Result) : nullptr;
}

std::optional<int>
directive_parser::LineDirective()
{
    token LineNumber = Lexer->NextNonWhitespace();
    if (LineNumber.Type == token_type::EOS)
    {
        Listener->Add(
                std::make_unique<error>(LineNumber.Location,
                                        "unexpected end of file after #line",
                                        Line));
    }
    else if (LineNumber.Type != token_type::PP_NUMBER)
    {
        Listener->Add(std::make_unique<error>(
                LineNumber.Location,
                Quote(LineNumber.Lexeme) +
                        " after #line is not a positive integer",
                Line));
    }
    else
    {
        int Number = 0;
        const bool IsPositiveInteger =
                ParsePositiveInteger(LineNumber.Lexeme, Number);
        if (!IsPositiveInteger)
        {
            Listener->Add(std::make_unique<error>(
                    LineNumber.Location,
                    Quote(LineNumber.Lexeme) +
                            " after #line is not a positive integer",
                    Line));
        }
        else
            return std::optional(Number);
    }
    return std::optional<int>();
}

void
directive_parser::ParseMacroArguments(std::vector<token>& IdentifierList)
{
    if (Current.Type == token_type::LPAREN)
    {
        Current = Lexer->Next();
        while (Current.Type != token_type::RPAREN)
        {
            if (Current.Type == token_type::IDENTIFIER)
                IdentifierList.push_back(Current);
            Current = Lexer->NextNonWhitespace();
        }
        Current = Lexer->Next();
    }
}

void
directive_parser::WarnAboutMissingSpaceAfterMacroName()
{
    Current = Lexer->Next();
    if (Current.Type != token_type::SPACE && Current.Type != token_type::LPAREN)
        Listener->Add(std::make_unique<warning>(
                Current.Location,
                "missing whitespace after the macro name",
                Line));
    if (Current.Type == token_type::SPACE) Current = Lexer->NextNonWhitespace();
}

void
directive_parser::ParseReplacementList(std::vector<token>& ReplacementList)
{
    while (Current.Type != token_type::NEWLINE &&
           Current.Type != token_type::EOS)
    {
        ReplacementList.push_back(Current);
        Current = Lexer->Next();
    }
}
