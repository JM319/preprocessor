#include "diagnostic_listener.h"

#include <iostream>
#include <sstream>

#include "diagnostic.h"

void
diagnostic_listener::Add(std::unique_ptr<diagnostic> Diagnostic)
{
    if (Diagnostic->GetType() == "warning")
        ++NumWarnings;
    else if (Diagnostic->GetType() == "error")
        ++NumErrors;
    else
        throw std::runtime_error(std::string("Unhandled Diagnostic type: ") +
                                 Diagnostic->GetType());
    Diagnostics.push_back(std::move(Diagnostic));
}

unsigned
diagnostic_listener::ErrorCount() const
{
    return NumErrors;
}

unsigned
diagnostic_listener::WarningCount() const
{
    return NumWarnings;
}

void
diagnostic_listener::Flush(std::ostream& Stream) const
{
    for (const auto& Diagnostic : Diagnostics)
        Stream << Diagnostic->ToString() << "\n";
}

std::string
diagnostic_listener::ToString() const
{
    std::stringstream Stream;
    Flush(Stream);
    Stream.seekg(0);

    return Stream.str();
}
